//
//  GameScene.swift
//  CommunicationTest
//
//  Created by Giselle Tavares on 2019-11-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit
import WatchConnectivity
import Firebase

class GameScene: SKScene, WCSessionDelegate {
    
    var ref: DatabaseReference! = Database.database().reference()
    
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed: "roll")
    let timerBar = SKSpriteNode(imageNamed: "life")

    // Make a tower
    var sushiTower:[SushiPiece] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    var catPosition: String = "left"
    
    // Show life and score labels
    let lifeLabel = SKLabelNode(text: "Lives: ")
    let scoreLabel = SKLabelNode(text: "Score: ")
    let resumeGameLabel = SKLabelNode(text: "")
    let highScoreTitleLabel = SKLabelNode(text: "High Scores")
    let highScoresLabel = SKLabelNode(text: "")
    
    var lives = 5
    var score = 0
    var playerName = String()
    var playerId: String! = ""
    
    var isGameRunning: Bool = false
    var numLoop: Int = 0
    var timerBarWidth: Int = 100
    var maxSeconds: Int = 25
    var countSeconds: Int = 0
    var countPowerUps: Int = 0
    var powerUp: Int = 0
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
            
            // 1. When a message is received from the watch, output a message to the UI
            // NOTE: Since session() runs in background, you cannot directly update UI from the background thread.
            // Therefore, you need to wrap any UI updates inside a DispatchQueue for it to work properly.
            DispatchQueue.main.async {
                if message["status"] as! String == "left" || message["status"] as! String == "right" {
                    self.catPosition = message["status"] as! String
                    self.resumeGame()
                } else if message["status"] as! String == "powerUp" {
                    self.updateTimerWithPowerUp()
                    self.resumeGame()
                } else if message["status"] as! String == "pause" {
                    self.pauseGame()
                } else {
                    self.playerName = message["status"] as! String
                    self.saveScore()
                }
            }
    }
    
    override func didMove(to view: SKView) {
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width * 0.25, y: 100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width * 0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
        
        // Game labels
        self.scoreLabel.position.x = 20
        self.scoreLabel.position.y = size.height - 150
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.fontSize = 20
        self.scoreLabel.horizontalAlignmentMode = .left
        addChild(self.scoreLabel)
        
        // Life label
        self.lifeLabel.position.x = 20
        self.lifeLabel.position.y = size.height - 200
        self.lifeLabel.fontName = "Avenir"
        self.lifeLabel.fontSize = 20
        self.lifeLabel.horizontalAlignmentMode = .left
        addChild(self.lifeLabel)
        
        self.lifeLabel.text = "Lives: \(self.lives)"
        self.scoreLabel.text = "Score: \(self.score)"
                   
        // Timer bar
        self.buildTimerBar()
        
        // Show High Scores
        self.showHighScores()
        
        self.moveCat()
        self.updateSushiTowerGraphics()
        self.checkGameOverOrScore()
        
        self.checkWCSessionSupport()
        
        self.powerUp = Int.random(in: 1 ..< 15)
        
    }
    
    func updateTimerWithPowerUp(){
        self.timerBar.size.width = self.timerBar.size.width + CGFloat((4 * self.countSeconds))
        self.timerBar.position.x = self.timerBar.position.x + CGFloat((2 * self.countSeconds))
        
        self.countSeconds -= 10
        
        if self.countSeconds < 0 {
            self.countSeconds = 0
            self.timerBar.size.width = 100
            self.timerBar.position.x = 80
        }
    }
    
    func pauseGame(){
        self.isGameRunning = false
        
        self.resumeGameLabel.text = "Touch to Resume Game"
        self.resumeGameLabel.position.x = size.width / 2
        self.resumeGameLabel.position.y = size.height / 2
        self.resumeGameLabel.fontName = "Avenir"
        self.resumeGameLabel.fontSize = 35
        self.resumeGameLabel.fontColor = .magenta
        self.resumeGameLabel.horizontalAlignmentMode = .center
        self.resumeGameLabel.zPosition = 2
        addChild(self.resumeGameLabel)
    }
    
    func resumeGame(){
        self.isGameRunning = true
        self.resumeGameLabel.text = ""
        self.resumeGameLabel.removeFromParent()
        self.highScoresLabel.removeFromParent()
        self.highScoreTitleLabel.removeFromParent()
    }
    
    func saveScore(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        
        self.playerId = formatter.string(from: date)
        
        // save score on Firebase
        self.ref.child("scores").child(self.playerId).setValue(
            ["playerName": String(self.playerName.prefix(3)),
             "score": self.score]
        )
        
        self.showHighScores()
    }
    
    func showHighScores(){
        self.isGameRunning = false
        
        self.highScoreTitleLabel.position.x = size.width / 2
        self.highScoreTitleLabel.position.y = size.height - 300
        self.highScoreTitleLabel.fontName = "Avenir"
        self.highScoreTitleLabel.fontSize = 40
        self.highScoreTitleLabel.fontColor = .yellow
        self.highScoreTitleLabel.horizontalAlignmentMode = .center
        self.highScoreTitleLabel.zPosition = 2
        addChild(self.highScoreTitleLabel)
        
        self.highScoresLabel.text = ""
        
        var count = 5
        var scoresArray = [String]()
        
        // fetching - because firebase is async, the result must be update inside it
        ref.child("scores").queryOrdered(byChild: "score").queryLimited(toLast: 5).observe(.childAdded, with: { (score) in
            let result = score.value as? [String : AnyObject]
            scoresArray.insert("\(count) - \(result!["score"]!) \(result!["playerName"]!)", at: scoresArray.startIndex)
            count -= 1
            
            var scoreList = ""
            for score in scoresArray {
                scoreList += "\(score)\n"
            }
            
            self.highScoresLabel.text = scoreList
        })

        self.highScoresLabel.position.x = size.width / 2
        self.highScoresLabel.position.y = size.height - 600
        self.highScoresLabel.fontName = "AvenirNext-Bold"
        self.highScoresLabel.fontSize = 40
        self.highScoresLabel.fontColor = .yellow
        self.highScoresLabel.horizontalAlignmentMode = .center
        self.highScoresLabel.zPosition = 2
        self.highScoresLabel.numberOfLines = 5
        addChild(self.highScoresLabel)
    }
    
    func spawnSushi() {
        
        // -----------------------
        // MARK: PART 1: ADD SUSHI TO GAME
        // -----------------------
        
        // 1. Make a sushi
        let sushi = SushiPiece(imageNamed:"roll")
        
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width * 0.5
        }
        else {
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width * 0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
    }
    
    func buildTimerBar(){
        let frameTimerBar = SKSpriteNode(imageNamed:"life_bg")
        frameTimerBar.size.width = 120
        frameTimerBar.size.height = 40
        frameTimerBar.position.x = 80
        frameTimerBar.position.y = size.height - 100
        frameTimerBar.zPosition = 1
        addChild(frameTimerBar)
        
        self.timerBar.size.width = 100
        self.timerBar.size.height = 20
        self.timerBar.position.x = 80
        self.timerBar.position.y = size.height - 100
        self.timerBar.zPosition = 2
        addChild(self.timerBar)
    }
    
    func checkWCSessionSupport(){
        if WCSession.isSupported() {
            print("\nGS Phone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            print("\nGS Session activated")
        }
        else {
            print("GS Phone does not support WCSession")
        }
    }
    
    func buildTower() {
        for _ in 0...10 {
            self.spawnSushi()
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        
        self.numLoop += 1
        
        if self.isGameRunning && self.numLoop % 60 == 0 { // each one second
            self.moveCat()
            self.updateSushiTowerGraphics()
            self.checkGameOverOrScore()
            
            self.countSeconds += 1
            self.timerBar.size.width -= 4
            self.timerBar.position.x -= 2
            
            switch self.countSeconds {
                case 10:
                    sendTimerStatusWatch(value: 15)
                case 15:
                    sendTimerStatusWatch(value: 10)
                case 20:
                    sendTimerStatusWatch(value: 5)
                default: break
            }
            
            if self.countSeconds >= self.maxSeconds {
                self.isGameRunning = false
                self.sendGameOverMessage()
            }
            
            print("powerUp second: \(self.powerUp)")
            if self.countSeconds == self.powerUp && self.countPowerUps <= 2 {
                self.countPowerUps += 1
                self.powerUp = Int.random(in: self.countSeconds ..< 23)
                self.sendPowerUpWatch()
            }
        }
    }
    
    func sendPowerUpWatch(){
        if WCSession.default.isReachable {
            print("Sending message to watch")
            WCSession.default.sendMessage(
                ["status" : "powerUp"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        } else {
            print("Watch is not reachable")
        }
    }
    
    func sendTimerStatusWatch(value: Int){
        if WCSession.default.isReachable {
            print("Sending message to watch")
            WCSession.default.sendMessage(
                ["status" : "\(value) seconds remaning"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        } else {
            print("Watch is not reachable")
        }
    }
    
    func sendGameOverMessage(){
        if !self.isGameRunning && (self.countSeconds >= self.maxSeconds || self.lives <= 0) {
            if WCSession.default.isReachable {
                print("Sending message to watch")
                WCSession.default.sendMessage(
                    ["status" : "gameOver"],
                    replyHandler: {
                        (_ replyMessage: [String: Any]) in
                        // @TODO: Put some stuff in here to handle any responses from the PHONE
                        print("Message sent")
                }, errorHandler: { (error) in
                    //@TODO: What do if you get an error
                    print("Error while sending message: \(error)")
                })
            } else {
                print("Watch is not reachable")
            }
        }
    }
    
    func updateSushiTowerGraphics(){
        // ------------------------------------
        // MARK: UPDATE THE SUSHI TOWER GRAPHICS
        //  When person taps mouse,
        //  remove a piece from the tower & redraw the tower
        // -------------------------------------
        let pieceToRemove = self.sushiTower.first
        if (pieceToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // To make the tower inifnite, then ADD a new piece
            self.spawnSushi()
        }
    }
    
    func moveCat(){
        // ------------------------------------
        // MARK: SWAP THE LEFT & RIGHT POSITION OF THE CAT
        //  If person taps left side, then move cat left
        //  If person taps right side, move cat right
        // -------------------------------------
        
        // 1. detect where person clicked
        if (self.catPosition == "left") {
            cat.position = CGPoint(x: self.size.width * 0.25, y: 100)
            
            // change the cat's direction
            let facingRight = SKAction.scaleX(to: 1, duration: 0)
            self.cat.run(facingRight)
        } else {
            cat.position = CGPoint(x: self.size.width * 0.85, y: 100)
            
            // change the cat's direction
            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
            self.cat.run(facingLeft)
        }

        // ------------------------------------
        // MARK: ANIMATION OF PUNCHING CAT
        // -------------------------------------
        
        // show animation of cat punching tower
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)
        
    }
    
    func checkGameOverOrScore(){
        // ------------------------------------
        // MARK: WIN AND LOSE CONDITIONS
        // -------------------------------------
        
        if (self.sushiTower.count > 0) {
            let firstSushi:SushiPiece = self.sushiTower[0]
            let chopstickPosition = firstSushi.stickPosition
            
            if (catPosition == chopstickPosition) {
                self.lives = self.lives - 1
                self.lifeLabel.text = "Lives: \(self.lives)"
                
            } else if (catPosition != chopstickPosition) {
                self.score = self.score + 10
                self.scoreLabel.text = "Score: \(self.score)"
            }
            
        } else {
            print("Sushi tower is empty!")
        }
        
        if self.lives <= 0 {
            print("game over")
            self.lives = 0
            self.lifeLabel.text = "Lives: \(self.lives)"
            self.isGameRunning = false
            self.sendGameOverMessage()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // if the game is paused
        if !self.isGameRunning {
            self.resumeGame()
        }
    }
 
}
