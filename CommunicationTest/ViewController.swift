//
//  ViewController.swift
//  CommunicationTest
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity
import SpriteKit
import GameplayKit

class ViewController: UIViewController {
    
    var catPosition = String()
    var scene = GameScene()
    var skView = SKView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.callGameScene()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callGameScene(){
        // Call the GameScene
        self.scene = GameScene(size: self.view.bounds.size)
        self.skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        self.scene.scaleMode = .aspectFill
        
        skView.presentScene(self.scene)
    }

}

