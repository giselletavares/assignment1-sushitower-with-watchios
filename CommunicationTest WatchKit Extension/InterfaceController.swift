//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//


import WatchKit
import Foundation
import WatchConnectivity


@available(watchOSApplicationExtension 6.0, *)
class InterfaceController: WKInterfaceController, WCSessionDelegate, WKExtensionDelegate {
    
    @IBOutlet weak var lblTimeStatus: WKInterfaceLabel!
    @IBOutlet weak var imgGameOver: WKInterfaceImage!
    @IBOutlet weak var grpTap: WKInterfaceGroup!
    @IBOutlet var btnPowerUp: WKInterfaceButton!
    @IBOutlet var txtName: WKInterfaceTextField!
    @IBOutlet var grpSaveScore: WKInterfaceGroup!
    @IBOutlet var lblScoreSaved: WKInterfaceLabel!
    
    
    var screenWidth = CGFloat()
    var sideTouched = ""
    var myTimer : Timer?
    var duration : TimeInterval = 2.0
    var playerName: String! = ""
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        print("WATCH: Got message from Phone")
        
        DispatchQueue.main.async {
            self.lblTimeStatus.setText("\(message["status"]! as! String)")
            
            if message["status"]! as! String == "gameOver" {
                self.isTimeOver()
            }
            
            if message["status"]! as! String == "powerUp" {
                self.runningTime()
            }
        }
    }
    
    func runningTime(){
        self.btnPowerUp.setHidden(false)
        Timer.scheduledTimer(withTimeInterval: self.duration, repeats: false) { (Timer) in
            self.btnPowerUp.setHidden(true)
        }
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        self.screenWidth = WKInterfaceDevice.current().screenBounds.width
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    @IBAction func didPlayerTap(_ sender: WKTapGestureRecognizer) {
        
        let location = sender.locationInObject()
        
        if location.x < self.screenWidth / 2 {
            self.sideTouched = "left"
        } else {
            self.sideTouched = "right"
        }
        
        self.touchedSide()
        
        print("\(self.screenWidth)")
        print("\(location)")
    }
    
    func touchedSide(){
        if WCSession.default.isReachable {
            print("Sending message to phone")
            WCSession.default.sendMessage(
                ["status" : self.sideTouched],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        }
        else {
            print("Phone is not reachable")
        }
    }
    
    func isTimeOver(){
        self.imgGameOver.setHidden(false)
        self.grpTap.setHidden(true)
        self.lblTimeStatus.setHidden(true)
        self.btnPowerUp.setHidden(true)
        self.grpSaveScore.setHidden(false)
    }
    
    @IBAction func powerUpButtonPressed() {
        lblTimeStatus.setText("Power Up added!")
        self.btnPowerUp.setHidden(true)
        
        if WCSession.default.isReachable {
            print("Sending message to phone")
            WCSession.default.sendMessage(
                ["status" : "powerUp"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        }
        else {
            print("Phone is not reachable")
        }
    }
    
    
    @IBAction func swipeToPause(_ sender: WKSwipeGestureRecognizer) {
        
        if WCSession.default.isReachable {
            print("Sending message to phone")
            WCSession.default.sendMessage(
                ["status" : "pause"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        } else {
            print("Phone is not reachable")
        }
    }
    
    
    @IBAction func getTxtName(_ value: NSString?) {
        self.playerName = value as String? ?? "NAN"
    }
    
    @IBAction func saveScorePressed() {
        self.lblScoreSaved.setHidden(false)
        self.lblScoreSaved.setText("Score Saved!")
        self.grpSaveScore.setHidden(true)
        
        if WCSession.default.isReachable {
            print("Sending message to phone")
            WCSession.default.sendMessage(
                ["status" : "\(self.playerName!)"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        } else {
            print("Phone is not reachable")
        }
    }
    
    

}

