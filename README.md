# SUSHI TOWER WITH iPHONE AND WATCH

* Giselle Cristina Moreno Tavares - C0744277

## Simulators

	* iPhone 11 Pro Max
	* watch series 5 - 44mm

## Pause / Resume game
To pause: on Watch, swipe to pause
To resume: on Phone, touch on the screen to resume (a message will appear indicating it)



-----
P.S.: I started this project with a brand new project, and I started to commit with this one. But because some errors on communication between watch and iphone suing SWIFT 5, etc. (it's a known SDK problem), I decided to take an old project with all configurations needed to have it working, just copying and pasting what I had done so far. This is the reason for the old commits disappeared.




